import { createStore, compose, applyMiddleware } from 'redux';
import ReduxThunk, { ThunkAction as ThunkActionOriginal } from 'redux-thunk'
import { AnyAction, combineReducers } from 'redux'

import { MainReducer } from '~/store/_blank'

let composeEnhancers = compose
const middlewares = [ ReduxThunk ]

if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
	composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
}

export interface IStore {
	main: ReturnType<typeof MainReducer>
}

const rootReducer = combineReducers<IStore>({
  main: MainReducer,
})

export interface IActions<T, P> {
	type: T
	payload: P
}

export type ThunkAction<T extends AnyAction> = ThunkActionOriginal<Promise<void>, IStore, null, T>


const configureStore = (initialState?: IStore) => {
	return createStore(
		rootReducer,
		initialState,
		composeEnhancers(
			applyMiddleware(...middlewares)
		)
	)
}

export const store = configureStore()
export default store