import { IActions } from '~/store'
// ˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉ
// model
// ˍˍˍˍˍˍˍˍˍˍˍˍˍˍˍ
const CHANGE_LOADER_STATUS = 'CHANGE_LOADER_STATUS'

export interface ILoaderParam {
  notSmzForm?: boolean
  contentBlock?: boolean
  profile?: boolean
}

export interface IMainState {
  notSmzForm: boolean
  contentBlock: boolean
  profile: boolean
}

export type changeMaintatusAction = IActions<typeof CHANGE_LOADER_STATUS, ILoaderParam>

// ˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉ
// Actions
// ˍˍˍˍˍˍˍˍˍˍˍˍˍˍˍ

export function changeMaintatus(param:ILoaderParam):changeMaintatusAction {
  return {
    type: CHANGE_LOADER_STATUS,
    payload: param
  }
}

// ˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉ
// Reducer
// ˍˍˍˍˍˍˍˍˍˍˍˍˍˍˍ

const initialState:IMainState = {
  profile: false,
  contentBlock: false,
  notSmzForm: false
}

export function MainReducer (state = initialState, action: changeMaintatusAction) {
  switch (action.type) {
    case CHANGE_LOADER_STATUS:
     return Object.assign({}, state, action.payload)
    default:
      return state
  }
}