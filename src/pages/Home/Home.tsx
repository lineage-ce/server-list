import React, { FunctionComponent, useEffect } from 'react'
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { IHomeProps } from './Home.model'
import { Button, Collapse } from 'antd'
import { IStore } from '~/store'

export const Home: FunctionComponent<IHomeProps> = () => {

	return (
		<div className="page home">
			<h1 className="page__title">Главная</h1>
			
			<div className="page__container">
				sdsd
			</div>
		</div>
	)
}

export default Home