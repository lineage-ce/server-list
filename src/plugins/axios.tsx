import React from 'react'
import axios, { AxiosRequestConfig, AxiosInstance, AxiosResponse } from 'axios'
import { notification } from 'antd'

const instance: AxiosInstance = axios.create({
  baseURL: '/api/npd/v1',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  }
})

const Request = (config: AxiosRequestConfig) => {
  const token = localStorage.getItem('npd_token')
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`
  }
  return config
}

const RequestError = (error: any) => {
  return Promise.reject(error)
}

const Response = (response: AxiosResponse<any>) => {
  return response
}

const ResponseError = (error: any) => {
  const data = error?.response?.data
  
  if (data && typeof data !== 'string') {
    return Promise.reject(error)
  }

  notification.error({
    message: 'Критическая ошибка',
    description: 'Обратитесь за помощью в службу технической поддержки',
  })
}

instance.interceptors.request.use(Request, RequestError)
instance.interceptors.response.use(Response, ResponseError)

export default instance