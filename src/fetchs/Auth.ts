import axios from '~/plugins/axios'

export function $_getToken(param: any) {
	return axios({
		url: `/oauth/${param.code}`,
		method: 'get'
	})
}