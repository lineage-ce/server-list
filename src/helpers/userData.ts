
export interface IUserDataLocalStorage {
	token: string
	userId: string
}