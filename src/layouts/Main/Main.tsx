import React, { FunctionComponent, useEffect } from 'react'
import { IMainProps } from './model'
import { Routes } from '~/routes'
import { Modal, Spin } from 'antd'

export const Main: FunctionComponent<IMainProps> = () => {
  return (
    <Spin spinning={false} tip="Загружаю профайл..." wrapperClassName="content-loader">
      <div className="main">
        <Routes />
      </div>
    </Spin>
  )
}

export default Main