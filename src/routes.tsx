import React, { FC } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home/Home'

export const Routes:FC = ()=> {
	return (
		<Switch>
			<Route exact path="/" component={Home} />
		</Switch>
	)
}

export default Routes