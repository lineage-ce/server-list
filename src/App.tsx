import React, { FunctionComponent, useEffect } from 'react';
import { Main } from './layouts/Main/Main'
import { BrowserRouter as Router } from 'react-router-dom'
import { matchMedia } from './plugins/matchMedia'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { IStore } from './store'

const App: FunctionComponent = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    matchMedia(dispatch)
  }, [])

  return (
    <div className="App">
      <Router>
        <Main />
      </Router>
    </div>
  )
}

export default App
