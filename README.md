
## О проекте

Язык: TypeScript
Фремворк: React (cra)
СтейтМенеджер: Redux/thunk
МокСервер: Express


### `yarn start`
Запуск в дев режиме

### `yarn build`
Компиляция статики в папку build

### `yarn moc`
Запуск мок сервера

Моки лежат в папке `./mocups`
  
