#!/usr/bin/env bash

function pathByVersion {
    filename=$(basename -- "$FRONTEND_SETTINGS_WEBROOT_TARGET")
    dir=$(dirname -- "$FRONTEND_SETTINGS_WEBROOT_TARGET")
    extension="${filename##*.}"
    filename="${filename%.*}"
    if [ ! -z "$dir" ]
    then
        echo "${dir}/${filename}.${APP_VERSION}.${extension}"
    else
        echo "${filename}.${APP_VERSION}.${extension}"
    fi
}

if [ -f "$FRONTEND_SETTINGS_SOURCE" ] && [ ! -z "$FRONTEND_SETTINGS_WEBROOT_TARGET" ]
then
    FRONTEND_SETTINGS_TARGET="${FRONTEND_WEBROOT_PATH}$(pathByVersion)"
    echo "$FRONTEND_SETTINGS_SOURCE => $FRONTEND_SETTINGS_TARGET"
    cp $FRONTEND_SETTINGS_SOURCE $FRONTEND_SETTINGS_TARGET
    echo $FRONTEND_SETTINGS_TARGET
    echo "--------------------"
    /usr/local/bin/ep -v $FRONTEND_SETTINGS_TARGET
    echo "--------------------"
    sed -i "s|${FRONTEND_SETTINGS_WEBROOT_TARGET}|$(pathByVersion)|g" ${FRONTEND_WEBROOT_PATH}/index.html
    echo "sed -i s|${FRONTEND_SETTINGS_WEBROOT_TARGET}|$(pathByVersion)|g ${FRONTEND_WEBROOT_PATH}/index.html"

fi

#cat /var/www/html/index.html
nginx -g "daemon off;"
