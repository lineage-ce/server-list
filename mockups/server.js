const Profile = require('./Profile/Profile')
const ProfileNew = require('./Profile/New')
const ProfileNotSmz = require('./Profile/NotSmz')
const Notification = require('./Notification/Notification')
const Dashboard = require('./Dashboard/Dashboard')
const Oktmo = require('./Dictionary/Oktmo')

const express = require('express')
const app = express()

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
  next()
})

app.get('/api/npd/v1/profile', Profile)
app.put('/api/npd/v1/profile/new', ProfileNew)
app.put('/api/npd/v1/profile/notSmz', ProfileNotSmz)


app.get('/api/npd/v1/notification', Notification)

app.get('/api/npd/v1/dashboard', Dashboard)

app.get('/api/npd/v1/dictionary/oktmo', Oktmo)



app.listen(3001, () => console.log(`Mockup server is run http://localhost:3001!`))