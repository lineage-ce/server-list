
const error = {
  'status': 500,
  'detail': 'Ошибка',
  'instance': 'mockup'
}

const data = {
  "TaxInfo": { 
    "Amount": 15.0 
  },
  "DebtInfo": {
    "Amount": 13.9,
    "DueDate": "2020-01-13"
  },
  "IncomeInfo":{
    "Amount": 150.0
  }
}

module.exports = function (req, res) {
  setTimeout(function(){
    res.send(data)
  }, 1000)
}