
const error = {
  'status': 500,
  'detail': 'Ошибка профайл не доступен',
  'instance': 'mockup'
}

const data = {
  "Id": "5a172027-c748-4c2c-83c7-640959b511c8",
  "Inn": "1234567890",
  "StatusSmz": "WaitApprove",
  "StatusIdentification": "Full",
  "ExternalUserId": "123456789012",
  "ExternalSystemId": "w1",
  "RegisteredByUs": true,
  "PlutoniumUserId": null,
  "UserAttributes": [
    {
        "Name": "FirstName",
        "Value": "Sergey",
        "VerificationStatus": "None"
    },
    {
        "Name": "LastName",
        "Value": "Gromov",
        "VerificationStatus": "None"
    },
    {
        "Name": "Patronymic",
        "Value": "Gennadievich",
        "VerificationStatus": "None"
    },
    {
        "Name": "Email",
        "Value": "sergey@gromov.io",
        "VerificationStatus": "None"
    },
    {
        "Name": "PhoneNumber",
        "Value": "76545865426",
        "VerificationStatus": "None"
    },
    {
        "Name": "Skype",
        "Value": "asddsadas",
        "VerificationStatus": "None"
    },
    {
        "Name": "Viber",
        "Value": "76545865426",
        "VerificationStatus": "None"
    }

  ]
}

module.exports = function (req, res) {
  setTimeout(function(){
    res.send(data)
  }, 1000)
}