
const error = {
  'status': 500,
  'detail': 'Ошибка ',
  'instance': 'mockup'
}

const data = {
  "messages": [
    {
      "Id":"a83c35b6-d128-4600-a316-48cda459f39d",
      "ExternalId":"9743f323-2a2c-40ab-9900-0d008ab96f22",
      "Inn":"1234567890",
      "Title":"1LOREM IPSUM",
      "Message":"lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum",
      "Status":"New",
      "CreationDate":"2019-12-27T20:21:46.801767+06:00",
      "LocalStatus":"New"
    },
    {
      "Id":"cfd80410-a299-4a2b-909c-3d6dc7f3992b",
      "ExternalId":"90498149-542a-4c8f-a37a-094eef7ea9bf",
      "Inn":"1234567890",
      "Title":"2LOREM IPSUM",
      "Message":"lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum",
      "Status":"Acknowledged",
      "CreationDate":"2019-12-27T20:21:46.85143+06:00",
      "LocalStatus":"Acknowledged"
    },
    {
      "Id":"9de445d0-a61c-4fea-973d-259edb88d6ad",
      "ExternalId":"14ad77af-d733-49c4-bc4f-4be177dfd15e",
      "Inn":"1234567890",
      "Title":"3LOREM IPSUM",
      "Message":"lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum",
      "Status":"Archived",
      "CreationDate":"2019-12-27T20:21:46.852208+06:00",
      "LocalStatus":"Archived"
    }
  ],
  "limit": 10,
  "offset": 0
}

module.exports = function (req, res) {
  setTimeout(function(){
    res.send(data)
  }, 1000)
}